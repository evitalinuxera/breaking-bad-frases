import { Fragment, useState } from 'react';
import { Container, Image, Button, Row, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import logo from "./images/logo.png";
import personajes from "./images/personajes.png";
import cocinando from "./images/cocinando.png";
import Frase from './components/Frase';

function App() {

// Hago un state de frases, arranco con un objeto vacío
// porque las frases vienen como objeto JSON desde la API

const [frase, setFrase] = useState({});

// Consultar la API y traerla 

  const consultarAPI = async () => {
        try{
        const api = await fetch('http://breaking-bad-quotes.herokuapp.com/v1/quotes');
        const frase = await api.json();
        setFrase(frase[0]);
        } catch (error) {
          console.log(error);
        }
};



  return (
    <Fragment>
    <Container>
      <Row>
        <Col>
          <Image 
           src={personajes} 
           rounded
        />
        </Col>
        <Col>
          <Image 
            src={logo} 
            thumbnail
          />
        </Col>
        <Col>
          <Image 
           src={cocinando} 
           roundedCircle
          />
        </Col>
      </Row>
    <div
    style={
      {
        paddingTop:"100px",
        display:"flex",
        flexDirection:"column",
        alignItems:"center"
      }
    }>
    <Row>
      <Col>
      <Button 
          variant="light"
          onClick={consultarAPI}
        >
        Traer Frase de Breaking Bad 
      </Button>
      </Col>
      </Row>
      <Row>
        <Col>
        <Frase
        frase = {frase}
        />
        </Col>
      </Row>
    </div>
    </Container>
    </Fragment>
  );
}

export default App;
