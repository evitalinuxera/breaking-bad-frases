import React from 'react';

const Frase = ({ frase }) => {
    return ( 
        <div style= {
            {
                margin: "1rem",
                padding: "3rem",
                borderRadius: ".9rem",
                backgroundColor: "#ffffff",
                maxWidth: "800px",
                color: "black"
            }
        }>
            {frase.quote}
            <p> - {frase.author} - </p>
        </div>
     );
}
 
export default Frase;