# README #

Este ejercicio se ha hecho consultando a una API que devuelve frases de la serie Breaking Bad

Se sugiere seguir ejercitando de la siguiente manera:

1) Usando un componente nuevo "Error" que aparezca cuando no se puede, por alguna causa, consultar a la API.

2) Corregir y mejorar el diseño.

3) Hacer que cuando cargue la página ya haya una frase.
